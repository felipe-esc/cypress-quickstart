# EndToEnd com Cypress - Implementação

Guia rápido para implementação de Cypress para testes end-to-end em projetos Angular(ou React, algum dia).
Baseado em: [Cypress Getting Started](https://docs.cypress.io/guides/getting-started)

## Instalação nos projetos

Para instalar o cypress em um projeto Angular2+ é só rodar um `npm install cypress --save-dev` (ou alternativamente `yarn add cypress --dev`).

## Rodando o Cypress

O framework é executado simplesmente com o comando `cypress run` em que ele abrirá um navegador e rodará os testes.
Pode-se alterar o comando `e2e` no `package.json` para `cypress run` e rodar no terminal `ng e2e` ou também adicionar um novo comando aos scripts da aplicação mais específico para o cypress com algum nome desejado (por exemplo adicionando `'cypress:run' : 'cypress run'` executar `npm cypress:run`).

## Escrevendo testes

Geralmente os arquivos de testes do Cypress estarão localizados dentro do diretório `cypress/integration/` e terão como extensão `.spec.js`, por exemplo `cypress/integration/example.spec.js`.

As etapas que orientam a escrita de um teste end-to-end:
1. Colocar a aplicação em algum estado (alguma página, por exemplo).
2. Realizar uma ou mais ações que alterem este estado inicial (navegação, cliques, submissão de formulários etc).
3. Verificar se as ações tomaram os efeitos pretendido.

## Escrevendo um teste

Vamos criar um teste de exemplo para demonstrar a estrutura básica de um teste (não fará quase nada e que falhará):
```javascript
describe('My First Test', () => {
  it('Tests my Hello World', () => {
    expect('Hello').to.equal('World');
  });
});
```
Em que, primeiramente, ele cria o teste propriamente dito, e em seguida descreve um caso de aceitação, fazendo uma comparação.
Abaixo exemplificaremos mais alguns exemplos, com alguns comandos do framework mais usados para se testar de verdade uma aplicação.

### Passo 1 - Visitando uma página

Para exemplificar um dos casos mais comuns de estado da aplicação, que é a visitação de uma página, vamos a um outro exemplo:

```javascript
describe('My First Test', () => {
  it('Visits home', () => {
    cy.visit('http://localhost:4200/home')
  });
});
```
Neste exemplo há apenas a navegação para a página desejada, sem nenhuma ação ou asserção.

### Passo 2 - Interagindo com a página

Aqui vamos já introduzir novas lógicas, como a busca por um elemento da página e uma interação simples com o mesmo. Também introduziremos uma função de set up do estado que queremos testar.

```javascript
describe('My First Test', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4200/home')
  });

  it('Clicks home button', () => {
    cy.contains('button').click();
  });

  it('Fills name input', () => {
    cy.get('.name')
      .type('fulano');
  });
});
```
Desta vez criamos uma função que navega diretamente para a página desejada e 2 testes diferentes, em que cada um realiza uma ação diferente. O primeiro apenas encontra um elemento `button` e o clica e o segundo apenas preenche um input com uma class `.email` com um email qualquer.


### Passo 3 - Verificando o efeito produzido

E, por fim, vamos adicionar alguma verificação para saber se há alguma mudança na página, e se essa é a pretendida. Escreveremos um teste em que ele entrará em nossa aplicação fictícia, navegará para a pagina de inscrição

```javascript
describe('My First Test', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4200/home')
  });

  afterEach(() => {
    /* Alguma lógica para terminar */
  });

  it('Subscribes to page feed', () => {
    cy.get('#subscribe').click();

    cy.url().should('include', '/subscribe');
  
    cy.get('.email')
      .type('fulano@email.com')
      .should('have.value', 'fulano@email.com');

    cy.find('button').then(($btn) => {
        if ($btn.hasClass('active')) {
            // faz alguma coisa
        } else {
            // faz alguma outra coisa
        }  
    });
});
```
Que verificamos se acessamos a página fictícia de subscribe e que conseguimos preencher o input com class `.email` e se possível clicar no botão de conclusão, podendo continuar com a lógica de testes ou não.
Há também funções como `before()` e `after()` para set up e tear down para chamadas únicas.

## Dicas

O framework possibilita a configurações de variáveis de ambiente e também de funções para serem reutilizáveis. Abaixo exemplificaremos as duas.

### Variáveis 

É possível criar variáveis de ambiente adicionando-se ao objeto padrão no arquivo ´/cypress.json´, por exemplo:
```javascript
{
    "baseUrl": "http://localhost:4200" 
}
```
E daí é possível utilizá-la diretamente com `cy.visit('/')` sem a necessidade de concatenação do path ou por meio de `Cypress.env("baseUrl")`.

### Funções

Também é muito útil definir funções que poderão ser reutilizadas no arquivo `cypress/support/commands.js`, por exemplo:

```javascript
Cypress.Commands.add("login", (*args) => { 
   /* Lógica de login na aplicação. */ 
});
// Ou também:
Cypress.Commands.overwrite("visit", (*args) => { 
   /* Outra lógica de navegação. */ 
});
```
Torna-se então possível chamar essa primeira função por meio de `cy.login(*args)`.

## Boas Práticas

- Não é interessante realizar sempre o login por meio da interface, sendo mais interessante testar uma única vez este fluxo e para os outros testes realizá-lo apenas com uma função fazendo a requisição devido ao custo.
- É interessante evitar buscar os elementos da página por meio de seletores como classes, ids e tags devido a possíveis mudanças nas estruturas do HTML sendo preferível usar a tag do framework `Data-*`(cy, test, testid)
- Buscar sempre testar o máximo possível de coisas em um testes, tentando cobrir ao máximo o fluxo, evitando-se fazer testes unitários.
- Evitar criar testes que dependem de outros para que estes passem, devido a possivel mudança em outros fluxos.
- Reutilizar código sempre que possível (principalmente lógicas que preparam o estado, limpam o estado etc)
- Tentar não esperar explicitamente por períodos de tempo por alguma mudança de estado que dependam de alguma lógica ou chamada assíncrona.

## Observações

O código Cypress roda assincronamente, detectando e esperando automaticamente algumas mudanças no DOM dependendendo do comando, por isso é importante observar o fluxo das páginas que se busca testar (a referência aos elementos podem mudar ou desaparecer conforme se navega, por exemplo).

Seria necessário também lembrar de colocar o diretório dos vídeos gerados das baterias de testes(`cypress/videos/`) no `.gitignore` a fim de não tornar o projeto muito pesado.

## Mais:
Para mais informações, guias e documentação visite a [página oficial do projeto](https://docs.cypress.io/)